/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */

/*
 * garfunkel.h
 * This file is part of Garfunkel
 *
 * Copyright (C) 2008 - Bruno Boaventura <brunobol@gnome.org>
 *
 * Garfunkel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Garfunkel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Garfunkel; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */
 
#ifndef __GARFUNKEL_H__
#define __GARFUNKEL_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GARFUNKEL_TYPE            (garfunkel_get_type ())
#define GARFUNKEL(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GARFUNKEL_TYPE, Garfunkel))
#define GARFUNKEL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass),  GARFUNKEL_TYPE, GarfunkelClass))
#define IS_GARFUNKEL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GARFUNKEL_TYPE))
#define IS_GARFUNKEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),  GARFUNKEL_TYPE))
#define GARFUNKEL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),  GARFUNKEL_TYPE, GarfunkelClass))

typedef struct _Garfunkel Garfunkel;
typedef struct _GarfunkelClass GarfunkelClass;
typedef struct _GarfunkelPrivate GarfunkelPrivate;

struct _Garfunkel
{
  GtkDrawingArea parent;
  GarfunkelPrivate *priv;
};

struct _GarfunkelClass
{
  GtkDrawingAreaClass parent_class;
};

GType garfunkel_get_type (void);

GtkWidget *garfunkel_new ();

G_END_DECLS

#endif /* __GARFUNKEL_H__ */
