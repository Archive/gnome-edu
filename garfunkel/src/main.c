/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */

/*
 * main.c
 * This file is part of Garfunkel
 *
 * Copyright (C) 2008 - Bruno Boaventura <brunobol@gnome.org>
 *
 * Garfunkel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Garfunkel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Garfunkel; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include "garfunkel.h"

int
main (int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *garfunkel;

  g_thread_init (NULL);
  gdk_threads_init ();

  gtk_set_locale ();
  gtk_init (&argc, &argv);
  
  gst_init (&argc, &argv);
  
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_show (window);

//  g_object_set (window,
//                "width-request", 300,
//                "height-request", 300,
//                NULL);

  g_signal_connect (window, "delete_event", gtk_main_quit, NULL);

  garfunkel = garfunkel_new ();

  gtk_container_add (GTK_CONTAINER (window), garfunkel);

  gtk_widget_show_all (window);

  gtk_widget_grab_focus (garfunkel);
  
  gdk_threads_enter ();
  gtk_main ();
  gdk_threads_leave ();

  return 0;
}

