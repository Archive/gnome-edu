/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */

/*
 * garfunkel.c
 * This file is part of Garfunkel
 *
 * Copyright (C) 2008 - Bruno Boaventura <brunobol@gnome.org>
 *
 * Garfunkel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Garfunkel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Garfunkel; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <librsvg/rsvg.h>
#include <glib-2.0/glib.h>

#include "garfunkel.h"
#include "garfunkel-sound.h"

#define GFK_BLINK_TIME  300000
#define THEME_FILENAME "garfunkel.svg"

G_DEFINE_TYPE (Garfunkel, garfunkel, GTK_TYPE_DRAWING_AREA);

typedef enum
{
  GFK_GREEN  = 1 << 0,
  GFK_RED    = 1 << 1,
  GFK_BLUE   = 1 << 2,
  GFK_YELLOW = 1 << 3
} GarfunkelLights;
  

struct _GarfunkelPrivate {
  RsvgHandle *svg_handle;

  GarfunkelLights lights;
  GarfunkelLights blink_once;
 
  GSList *sequence;
  GSList *user_sequence;
  
  GMutex *thread_mutex;
};

static          gboolean garfunkel_expose (GtkWidget *widget, GdkEventExpose *event);
static void     garfunkel_redraw (Garfunkel *garfunkel);
static void     garfunkel_blink (Garfunkel *garfunkel, GarfunkelLights light);
static void     garfunkel_sequence_increment (Garfunkel *garfunkel);
static void     garfunkel_sequence_drop (Garfunkel *garfunkel);
static void     garfunkel_sequence_blink (Garfunkel *garfunkel);
static void     garfunkel_sequence_blink_thread (gpointer data);
static void     garfunkel_blink_once (Garfunkel *garfunkel, GarfunkelLights light);
static void     garfunkel_blink_once_thread (gpointer data);

static gboolean garfunkel_key_press (GtkWidget *widget, GdkEventKey *event);
static gboolean garfunkel_button_press (GtkWidget *widget, GdkEventButton *event);


static void
garfunkel_init (Garfunkel* garfunkel)
{
  gtk_widget_set_events (GTK_WIDGET (garfunkel),
                         gtk_widget_get_events (GTK_WIDGET (garfunkel))
                         | GDK_KEY_PRESS_MASK
                         | GDK_BUTTON_PRESS_MASK);
  
  GTK_WIDGET_SET_FLAGS (GTK_WIDGET (garfunkel), GTK_CAN_FOCUS);
  
  garfunkel->priv = g_new0 (GarfunkelPrivate, 1);
  
  garfunkel->priv->lights = 0;
  garfunkel->priv->blink_once = 0;
  garfunkel->priv->sequence = NULL;
  garfunkel->priv->user_sequence = NULL;
  
  garfunkel->priv->thread_mutex = g_mutex_new ();
}


static void
garfunkel_finalize(GObject* object)
{
  Garfunkel * garfunkel = (Garfunkel *) object;

  g_object_unref (garfunkel->priv->svg_handle);

  g_mutex_free (garfunkel->priv->thread_mutex);
    
  g_free(garfunkel->priv);
}


static void
garfunkel_class_init (GarfunkelClass *klass)
{
  GObjectClass *obj_class;
  GtkWidgetClass *widget_class;

  obj_class = G_OBJECT_CLASS (klass);
  widget_class = GTK_WIDGET_CLASS (klass);

  obj_class->finalize = garfunkel_finalize;
  
  /* GtkWidget signals */
  widget_class->expose_event = garfunkel_expose;
  widget_class->key_press_event = garfunkel_key_press;
  widget_class->button_press_event = garfunkel_button_press;
}


GtkWidget *
garfunkel_new (void)
{
  Garfunkel * garfunkel;
  GarfunkelPrivate * gp= NULL;

  char *filename;
    
  garfunkel = GARFUNKEL (g_object_new (GARFUNKEL_TYPE, NULL));

  gp = garfunkel->priv;
           
  filename = g_build_filename (DEFAULT_THEME_DIR, THEME_FILENAME, NULL);
  
  gp->svg_handle = rsvg_handle_new_from_file (filename, NULL);
  
  g_free (filename);

  return (GTK_WIDGET (garfunkel));
}


static void
garfunkel_draw (GtkWidget *widget, cairo_t *cr)
{
  Garfunkel *garfunkel;
  GarfunkelPrivate * gp= NULL;

  garfunkel = GARFUNKEL (widget);

  gp = garfunkel->priv;

  double width;
  double height;

  double factor;
  
  width = widget->allocation.width;
  height = widget->allocation.height;

  factor = (width > height) ? height : width;

  /* Centering the draw */  
  cairo_translate(cr,
                  (width - (100 * (factor / 100))) / 2 ,
                  (height - (100 * (factor / 100))) / 2);
  
  /* Scaling */
  cairo_scale (cr, factor / 100.0, factor / 100.0); 

  rsvg_handle_render_cairo_sub (gp->svg_handle, cr, "#background");

  if (gp->lights & GFK_GREEN)
    rsvg_handle_render_cairo_sub (gp->svg_handle, cr, "#green-on");
  else
    rsvg_handle_render_cairo_sub (gp->svg_handle, cr, "#green");
  
  if (gp->lights & GFK_RED)
    rsvg_handle_render_cairo_sub (gp->svg_handle, cr, "#red-on");
  else
    rsvg_handle_render_cairo_sub (gp->svg_handle, cr, "#red");

  if (gp->lights & GFK_BLUE)
    rsvg_handle_render_cairo_sub (gp->svg_handle, cr, "#blue-on");
  else
    rsvg_handle_render_cairo_sub (gp->svg_handle, cr, "#blue");

  if (gp->lights & GFK_YELLOW)
    rsvg_handle_render_cairo_sub (gp->svg_handle, cr, "#yellow-on");
  else
    rsvg_handle_render_cairo_sub (gp->svg_handle, cr, "#yellow");
}


static gboolean
garfunkel_expose (GtkWidget *widget, GdkEventExpose *event)
{
  cairo_t *cr;

  cr =  gdk_cairo_create (widget->window);

  cairo_rectangle (cr,
                   event->area.x, event->area.y,
                   event->area.width, event->area.height);
  
  cairo_clip (cr);
  
  garfunkel_draw (widget, cr);

  cairo_destroy (cr);

  return FALSE;
}


static void
garfunkel_redraw (Garfunkel *garfunkel)
{
  gdk_threads_enter ();
  
  GtkWidget *widget;
  GdkRegion *region;

  widget = GTK_WIDGET (garfunkel);
  
  if (!widget->window) return;
  
  region = gdk_drawable_get_clip_region (widget->window);
  /* redraw the cairo canvas completely by exposing it */
  gdk_window_invalidate_region (widget->window, region, TRUE);
  gdk_window_process_updates (widget->window, TRUE);

  gdk_region_destroy (region);
  
  gdk_threads_leave ();
}


static void
garfunkel_blink (Garfunkel *garfunkel, GarfunkelLights light)
{
  GarfunkelPrivate * gp= NULL;

  gp = garfunkel->priv;

  gp->lights = garfunkel->priv->lights | light;

  garfunkel_redraw (garfunkel);

  gchar *filename;

  filename = g_build_filename (SOUND_DIR, "test.ogg", NULL);
  
  garfunkel_sound_play (filename);

  g_free (filename);

  g_usleep (GFK_BLINK_TIME);

  gp->lights = 0;

  garfunkel_redraw (garfunkel);
}


static void
garfunkel_blink_once (Garfunkel *garfunkel, GarfunkelLights light)
{
  GarfunkelPrivate * gp= NULL;

  gp = garfunkel->priv;
  
  if (!g_mutex_trylock (garfunkel->priv->thread_mutex))
    return;

  gp->lights = garfunkel->priv->blink_once | light;
  
  g_thread_create (garfunkel_blink_once_thread, (gpointer) garfunkel, FALSE, NULL);  
}


static void
garfunkel_blink_once_thread (gpointer data)
{
  Garfunkel *garfunkel = (Garfunkel *) data;

  GarfunkelPrivate * gp= NULL;

  gp = garfunkel->priv;

  garfunkel_blink (garfunkel, gp->blink_once);
  
  g_mutex_unlock (gp->thread_mutex);
}


static void
garfunkel_sequence_increment (Garfunkel *garfunkel)
{
  GarfunkelPrivate * gp= NULL;

  gp = garfunkel->priv;

  gint32 color;
  
  color = g_random_int_range (0, 4);
  
  gp->sequence = g_slist_append (gp->sequence, (gpointer) (1 << color));
}


static void
garfunkel_sequence_drop (Garfunkel *garfunkel)
{
  GarfunkelPrivate * gp= NULL;

  gp = garfunkel->priv;

  g_slist_free (gp->sequence);

  gp->sequence = NULL;
}


static void
garfunkel_sequence_blink (Garfunkel *garfunkel)
{
  if (!g_mutex_trylock (garfunkel->priv->thread_mutex))
    return;

  garfunkel_sequence_increment (garfunkel);
  
  g_thread_create (garfunkel_sequence_blink_thread, (gpointer) garfunkel, FALSE, NULL);
}


static void
garfunkel_sequence_blink_thread (gpointer data)
{
  Garfunkel *garfunkel = (Garfunkel *) data;

  GarfunkelPrivate * gp= NULL;

  gp = garfunkel->priv;

  GSList *tmplist;

  tmplist = gp->sequence;

  while (tmplist)
  {
  
    garfunkel_blink (garfunkel, (gint) (tmplist->data));
    
    g_usleep (GFK_BLINK_TIME);
    
    tmplist = g_slist_next (tmplist);
  }
  
  garfunkel_redraw (garfunkel);

  g_mutex_unlock (gp->thread_mutex);
} 


static gboolean
garfunkel_key_press (GtkWidget *widget, GdkEventKey *event)
{
  switch (event->keyval)
  {
    case GDK_a:
      garfunkel_blink_once ((Garfunkel *) widget, GFK_GREEN);      
    case GDK_s:
      garfunkel_blink_once ((Garfunkel *) widget, GFK_RED);      
    case GDK_z:
      garfunkel_blink_once ((Garfunkel *) widget, GFK_YELLOW);      
    case GDK_x:
      garfunkel_blink_once ((Garfunkel *) widget, GFK_BLUE);      
  }
}


static gboolean
garfunkel_button_press (GtkWidget *widget, GdkEventButton *event)
{
  Garfunkel *garfunkel;

  garfunkel = (Garfunkel *) widget;
  
  garfunkel_sequence_blink (garfunkel);

  return FALSE;
}

