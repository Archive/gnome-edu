/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */

/*
 * garfunkel-sound.h
 * This file is part of Garfunkel
 *
 * Copyright (C) 2009 - Bruno Boaventura <brunobol@gnome.org>
 *
 * Garfunkel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Garfunkel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Garfunkel; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */
 
/* This code is based on "libgames-support/games-sound.c" fom Andreas Røsdal */
 
#ifndef __GARFUNKEL_SOUND_H__
#define __GARFUNKEL_SOUND_H__

#include <glib.h>

G_BEGIN_DECLS

void garfunkel_sound_play (const gchar *filename);

G_END_DECLS

#endif /* __GARFUNKEL_SOUND_H__ */
