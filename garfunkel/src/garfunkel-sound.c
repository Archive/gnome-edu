/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */

/*
 * garfunkel-sound.c
 * This file is part of Garfunkel
 *
 * Copyright (C) 2009 - Bruno Boaventura <brunobol@gnome.org>
 *
 * Garfunkel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Garfunkel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Garfunkel; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */
 
/* This code is based on "libgames-support/games-sound.c" from Andreas Røsdal */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include <gst/gst.h>

#include "garfunkel-sound.h"

static gboolean sound_init = FALSE;

static GstElement *pipeline;

static void
garfunkel_sound_init (void)
{
  GError *err = NULL;

  g_assert (g_thread_supported ());

  pipeline = gst_element_factory_make ("playbin", "playbin");

  sound_init = TRUE;
}

void
garfunkel_sound_play (const gchar * filename)
{
  GError *err = NULL;

  if (!sound_init)
    garfunkel_sound_init ();

  char *uri;

  gboolean done = FALSE;

  GstBus *bus;

  bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));

  uri = g_strdup_printf ("file:///%s", filename);

  g_object_set (G_OBJECT (pipeline), "uri", uri, NULL);

  g_free (uri);

  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  do
    {
      GstMessage *message;

      message = gst_bus_timed_pop (bus, GST_CLOCK_TIME_NONE);

      switch (GST_MESSAGE_TYPE (message))
        {
          case GST_MESSAGE_EOS:
            done = TRUE;
            break;
          default:
            break;
        }

      gst_message_unref (message);

    } while (!done);

  gst_element_set_state (pipeline, GST_STATE_NULL);
}

